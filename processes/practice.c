#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
int main( void ) {
    char *argv[3] = {"Command", "-l", NULL};
    int p = fork();
    if ( p == 0 ) {
        execvp( "/bin/ls", argv );

    } else {
      wait(NULL);
      printf("Finished executing the parent process\n");
    }
    return 0;
    }
