#include <stdio.h>

#include <stdlib.h>

#include <pthread.h>

#include <semaphore.h>

#include <unistd.h>

#define N 10 // buffer capacity

struct node* head = NULL;   // head of the linked list
sem_t empty;
sem_t full;
pthread_mutex_t mtx;
int buffer = 0;             //buffer size

typedef struct node{    
  char val;
  struct node *next;
} node_t;


// function for inserting A at the head of the linked list
void push() {
  struct node* temp = (struct node*) malloc(sizeof(struct node)); 
  temp->val = 'A';                                                  // allocating new node with value A

  temp->next = head;
  head = temp;
}

//function to pop element at head
void pop() {
  head = head->next;
}

void * producer() {
  sem_wait( & empty);
  pthread_mutex_lock( & mtx);

  buffer += 1;    // incrementing buffer size
  push();         // pushing A into linked list

  struct node* temp = head;

  //printing producer message
  while(temp != NULL) {
    printf("%c", temp->val);
    temp = temp->next;
  }
  printf(" %d\n", buffer);

  pthread_mutex_unlock( & mtx);
  usleep(1000);
  sem_post( & full);
}
void * consumer() {
  sem_wait( & full);
  pthread_mutex_lock( & mtx);

  buffer -= 1;    //decrementing buffer size
  pop();          //poping element at the head of linked list (buffer)

  struct node* temp = head;

  //printng consumer message
  printf("[");
  while(temp != NULL) {
    printf("%c", temp->val);
    temp=temp->next;
  }
  printf("]");
  printf("[%d]\n", buffer);


  pthread_mutex_unlock( & mtx);
  usleep(1000);
  sem_post( & empty);
}

int main() {
  pthread_t prod, cons;
  pthread_mutex_init( & mtx, NULL);
  sem_init( & empty, 0, N);
  sem_init( & full, 0, 0);

  for (int i = 0; i < 10; i++) {
    pthread_create( & cons, NULL, (void * ) consumer, NULL);

    sleep(1);

    pthread_create( & prod, NULL, (void * ) producer, NULL);
pthread_join(prod, NULL);
    pthread_join(cons, NULL);
  
  }

  for (int i = 0; i < 10; ++i) {
    }
  pthread_mutex_destroy( & mtx);
  sem_destroy( & empty);
  sem_destroy( & full);
  return 0;
}