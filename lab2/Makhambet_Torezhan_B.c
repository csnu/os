#include <stdio.h>

#include <stdlib.h>

#include <pthread.h>

#include <semaphore.h>

#include <unistd.h>

#define N 10 // buffer capacity

typedef struct node{    
  char val;
  struct node *next;
} node_t;


struct node* head = NULL;   // head of the linked list
pthread_cond_t empty = PTHREAD_COND_INITIALIZER;
pthread_cond_t full = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mtx;
int buffer = 0;             //buffer size




// function for inserting A at the head of the linked list
void push() {
  struct node* temp = (struct node*) malloc(sizeof(struct node)); 
  temp->val = 'A';                                                  // allocating new node with value A

  temp->next = head;
  head = temp;
}

//function to pop element at head
void pop() {
  struct node* temp = head;
  head = head->next;
  free(temp);
}

void * producer() {
  
  pthread_mutex_lock( & mtx);


  if(buffer == N)   // buffer is full 
    pthread_cond_wait( & empty, &mtx);  // wait on empty conditional variable

  buffer += 1;    // incrementing buffer size
  push();         // pushing A into linked list

  struct node* temp = head;

  //printing producer message
  while(temp != NULL) {
    printf("%c", temp->val);
    temp = temp->next;
  }
  printf(" %d\n", buffer);

  pthread_mutex_unlock( & mtx);
  usleep(1000);                   // giving time to another thread
  pthread_cond_signal( & empty); // signal on empty conditional variable
}
void * consumer() {
  
  pthread_mutex_lock( & mtx);

  if(buffer == 0)     // buffer is empty
    pthread_cond_wait( & empty, &mtx); // wait on empty conditional variable
      
  buffer -= 1;    //decrementing buffer size
  pop();          //poping element at the head of linked list (buffer)

  struct node* temp = head;

  //printng consumer message
  printf("[");
  while(temp != NULL) {
    printf("%c", temp->val);
    temp=temp->next;
  }
  printf("]");
  printf("[%d]\n", buffer); // signal on empty conditional variable
  

  pthread_mutex_unlock( & mtx);
  usleep(1000);             // giving time to another thread
  pthread_cond_signal( & empty);
}

int main() {
  pthread_t prod, cons;
  pthread_mutex_init( & mtx, NULL);

  for (int i = 0; i < N; i++) {
    pthread_create( & cons, NULL, (void * ) consumer, NULL);

    sleep(1);

    pthread_create( & prod, NULL, (void * ) producer, NULL);
    pthread_join(prod, NULL);
    pthread_join(cons, NULL);
  
  }

  pthread_mutex_destroy( & mtx);
  return 0;
}