#include <stdio.h>

#include <stdlib.h>

#include <unistd.h>

#include <string.h>

#include <pthread.h>

#include <semaphore.h>

#include <unistd.h>

#define N 10 // buffer capacity

char arr[N];      // array that contains
int idx = 0;      // index of current element in arr
sem_t empty;
sem_t full;
pthread_mutex_t mtx;
int buffer = 0;   // buffer size

// function that shifts elements in arr by given start index
void remove_element(int index, int length) {
  
  for (int i = index; i < length - 1; i++)
    arr[i] = arr[i + 1];
}

void * producer() {
  sem_wait( & empty);
  pthread_mutex_lock( & mtx);

  buffer += 1;      //increment buffer size
  arr[idx] = 'A';   //inserting item to buffer
  idx++;

  //printing producer message, buffer size and its contents
  // starts from 1 to buffer size
  for (int i = 0; i < buffer; i++) {
    for (int j = 0; j < i + 1; j++)
      printf("%c", arr[j]);
    printf(" %d\n", i + 1);
  }

  pthread_mutex_unlock( & mtx);
  usleep(1000);         // giving time to another thread
  sem_post( & full);
}
void * consumer() {
  sem_wait( & full);
  pthread_mutex_lock( & mtx);

  remove_element(idx, buffer);  //removing item from buffer with given index
  idx--;
  buffer -= 1;              // decrementing buffer size

  // printing consumer message, buffer and its content
  // starts from buffer size to 0, 
  for (int i = buffer; i >= 0; i--) {
    printf("[");
    for (int j = 0; j < i; j++)
      printf("%c", arr[j]);
    printf("]");

    printf("[%d]\n", i);
  }

  pthread_mutex_unlock( & mtx);
  usleep(5000);     // giving time to another thread
  sem_post( & empty);
}

int main() {
  pthread_t prod, cons;
  pthread_mutex_init( & mtx, NULL);
  sem_init( & empty, 0, N);
  sem_init( & full, 0, 0);

  for (int i = 0; i < N; i++) {
    pthread_create( & cons, NULL, (void * ) consumer, NULL);

    sleep(1);

    pthread_create( & prod, NULL, (void * ) producer, NULL);
    
    pthread_join(prod, NULL);
    pthread_join(cons, NULL);
  }


  pthread_mutex_destroy( & mtx);
  sem_destroy( & empty);
  sem_destroy( & full);
  return 0;
}