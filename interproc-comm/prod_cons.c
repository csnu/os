#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#define N 10 // Buffer size
sem_t empty;
sem_t full;
pthread_mutex_t mutex;	
int buffer = 0;

void * producer() {
  sem_wait( & empty);
  pthread_mutex_lock( & mutex);
  buffer += 1;
  printf("Producer inserted 1 item, buffer items = %d\n", buffer);
  pthread_mutex_unlock( & mutex);
  sem_post( & full);
}


void * consumer() {
  sem_wait( & full);
  pthread_mutex_lock( & mutex);
  buffer -= 1;
  printf("Consumer removed 1 item, buffer items = %d\n", buffer);
  pthread_mutex_unlock( & mutex);
  sem_post( & empty);
}

int main() {
  pthread_t prod, cons;
  pthread_mutex_init( & mutex, NULL);
  sem_init( & empty, 0, N);
  sem_init( & full, 0, 0);
  for (int i = 0; i < 10; i++) {
    pthread_create( & prod, NULL, (void * ) producer, NULL);
    pthread_create( & cons, NULL, (void * ) consumer, NULL);
    pthread_join(prod, NULL);
    pthread_join(cons, NULL);
  }
  pthread_mutex_destroy( & mutex);
  sem_destroy( & empty);
  sem_destroy( & full);
  return 0;
}