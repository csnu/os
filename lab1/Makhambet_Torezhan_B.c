#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <semaphore.h>

sem_t mutex;
int count = 0;

void * flush(void * ptr) {

  char * s;
  s = (char * ) ptr;      // string casting
  int l = strlen(s);
  for (int i = 0; i < l; i++) {
    sem_wait(&mutex);   //decrements the semaphore

    while(count == 0)     // used for unlocking the semophore and synchronizing outputs
      sem_post(&mutex);
    count = 0;

    putchar(s[i]);      //outputing characters
    fflush(stdout);
    sleep(rand() % 2);   //sleeps 0 or 1 seconds

    sem_post(&mutex);   // increments the semophore or unlocks it
  }

  pthread_exit(NULL);
}

void * flushConsumer(void * ptr) {

  char * s;
  s = (char * ) ptr;
  int l = strlen(s);
  for (int i = 0; i < l; i++) {
    sem_wait(&mutex);

    while(count == 1)
      sem_post(&mutex);
    count = 1;

    putchar(s[i]);
    fflush(stdout);
    sleep(rand() % 2);

    sem_post(&mutex);
  }

  pthread_exit(NULL);
}


int main() {
  sem_init(&mutex, 0, 1);       // initializing semaphore
  char * s1 = "abcdefghij";
  char * s2 = "ABCDEFGHIJ";
  pthread_t t1, t2;
  int pid1 = pthread_create( & t1, NULL, flush, (void * ) s1);
  int pid2 = pthread_create( & t2, NULL, flushConsumer, (void * ) s2);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  sem_destroy(&mutex);
  printf("\n");
  return 0;
}
