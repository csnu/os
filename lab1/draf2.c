#include <stdio.h>

#include <stdlib.h>

#include <unistd.h>

#include <string.h>

#include <pthread.h>

#include <sched.h>
#include <semaphore.h>

sem_t mutex;
pthread_mutex_t lock;

int count = 0;

void * flush(void * ptr) {

  char * s;
  s = (char * ) ptr;
  int l = strlen(s);

    pthread_mutex_lock(&lock);
  for (int i = 0; i < l; i++) {


    putchar(s[i]);
    fflush(stdout);
    sleep(rand() % 2);


  }
pthread_mutex_unlock(&lock);
  pthread_exit(NULL);
}

void * flush2(void * ptr) {

  char * s;
  s = (char * ) ptr;
  int l = strlen(s);
pthread_mutex_lock(&lock);

  for (int i = 0; i < l; i++) {

    putchar(s[i]);
    fflush(stdout);
    sleep(rand() % 2);


  }
pthread_mutex_unlock(&lock);
  pthread_exit(NULL);
}


int main() {
  pthread_mutex_init(&lock, NULL);
  char * s1 = "abcdefghij";
  char * s2 = "ABCDEFGHIJ";
  pthread_t t1, t2;
  int pid1 = pthread_create( & t1, NULL, flush, (void * ) s1);
  int pid2 = pthread_create( & t2, NULL, flush2, (void * ) s2);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_mutex_destroy(&lock);
  printf("\n");
  return 0;
}
