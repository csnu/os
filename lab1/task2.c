#include <stdio.h>

#include <stdlib.h>

#include <unistd.h>

#include <string.h>

#include <pthread.h>

#include <sched.h>
#include <semaphore.h>

sem_t mutex;
pthread_mutex_t lock;

void * flush(void * ptr) {

  char * s;
  s = (char * ) ptr;
  int l = strlen(s);

  for (int i = 0; i < l; i++) {
    
    sem_wait(&mutex);
    pthread_mutex_lock(&lock);

    // the following 3 lines cannot be changed (content + order)
    putchar(s[i]);
    fflush(stdout);
    sleep(rand() % 2);

    pthread_mutex_unlock(&lock);
    sem_post(&mutex);
    
  }
}
int main() {
  sem_init(&mutex, 0, 1);
  pthread_mutex_init(&lock, NULL);
  
  char * s1 = "abcdefghij";
  char * s2 = "ABCDEFGHIJ";
  pthread_t t1, t2;
  int pid1 = pthread_create( & t1, NULL, flush, (void * ) s1);
  int pid2 = pthread_create( & t2, NULL, flush, (void * ) s2);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  sem_destroy(&mutex);
  pthread_mutex_destroy(&lock);
  printf("\n");
  return 0;
  //exit(0);
}
