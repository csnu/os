#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

int main( int argc, char *argv[] ) {
  FILE *p;
  char ch;

  if (argc != 2){
     printf("%s", "You must write the process name!\n");    // checking validitity of arguments. Must provide process name
     return -1;
  }

  char* process = argv[1];  // process name e.g. libreoffice, etc.


  char cmd_pidof[]= "pidof ";
  strcat(cmd_pidof, process); // now cmd_pidof = "pidof libreoffice" e.g.

  p = popen(cmd_pidof,"r");




  if(ch=fgetc(p) == EOF ){  // checking whether process exists in the process table in current time
    puts("Process does not exist in process table");
    return 1;
  }

  puts("The process exists in the process table with pids:");
  while( (ch=fgetc(p)) != EOF)
      putchar(ch);              // printing out pidof process name



  // task 1 WHEREIS OUTPUT
  int p1 = fork();
  if ( p1 == 0 ) {    // child created successfully
      char* arg[3];
      arg[0] = "whereis";
      arg[1] = process;
      arg[2] = NULL;
      printf("%s", "\nWhereis output:\n");
      execvp(arg[0], arg);      // executes command like "whereis libreoffice"
  } else {
    wait(NULL);
  }




  // task 2 manual of the process in another terminal window
  int p2 = fork();
  char cmd_man[100] = "man ";
  strcat(cmd_man, process); //combining strings. now for example cmd_man equals to 'man libreoffice'

  if ( p2 == 0 ) {  // child created successfully
      char* arg[4];
      arg[0] = "xfce4-terminal";
      arg[1] = "-e";
      arg[2] = cmd_man;
      arg[3] = NULL;

      execvp(arg[0], arg);   // executes command like "xfce4-termainal -e 'man libreoffice'"
  } else {
    wait(NULL);
  }






  // task 3 firefox google query of that process
  int p3 = fork();
  char cmd_query[100] = "google.com/search?query=";
  strcat(cmd_query, process);     // combining strings

  if ( p3 == 0 ) {  // child created successfully
      char* arg[3];
      arg[0] = "firefox";
      arg[1] = cmd_query;
      arg[2] = NULL;

      execvp(arg[0], arg);  // executes command like 'firefox google.com/search?query=libreoffice'
  } else {
    wait(NULL);
  }


  pclose(p);

  return 0;
}
