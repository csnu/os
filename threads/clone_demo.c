#define _GNU_SOURCE

#include <stdio.h>

#include <unistd.h>

#include <stdlib.h>

#include <sched.h>

#define STACK 8192
int do_something() {
  printf("Child pid : %d\n", getpid());
  exit(0);
}
int main() {
  void * stack = malloc(STACK); // Stack for new process
  int p = clone( & do_something, (char * ) stack + STACK, CLONE_VM, NULL);
  printf("Parent pid : %d\n", getpid());
  sleep(1);
  free(stack);
  return 0;
}