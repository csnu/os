#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sched.h>
void *print_message_function( void *ptr ){
	char *message;
	message = (char *) ptr;
	while(1) {
		printf("%s with id of %lu\n", message, pthread_self());
		sched_yield();

	}
	pthread_exit(NULL);
}
int main(){
	pthread_t thread1, thread2;
	char *message1 = "Hello from Thread 1";
	char *message2 = "Hello from Thread 2";
	int ret1 = pthread_create( &thread1, NULL, print_message_function, (void*) message1);
	int ret2 = pthread_create( &thread2, NULL, print_message_function, (void*) message2);

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);

	return 0;
}
