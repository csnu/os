#include <stdio.h>
#include <omp.h>
int main(int argc, char *argv[]){
	int sum = 0;
	int i =0;

	omp_set_num_threads(4);
	
	

	#pragma omp parallel
	{
		for(int i = 0; i < 100; i++) {
			sum += i;
		}
		//printf("I am a parallel region.\n");
	}

	printf("sum = %d.\n", sum);
	return 0;
}